from bonus_system import calculateBonuses

STANDARD = "Standard"
PREMIUM = "Premium"
DIAMOND = "Diamond"
NOTHING_ELSE_MATTERS = "Nothing"


def test_standard_program():
    process_test_with_program(STANDARD)


def test_premium_program():
    process_test_with_program(PREMIUM)


def test_diamond_program():
    process_test_with_program(DIAMOND)


def test_nothing_program():
    process_test_with_program(NOTHING_ELSE_MATTERS)


def process_test_with_program(program):
    if program == STANDARD:
        assert calculateBonuses(STANDARD, 1) == __multiplication(1, 0.5)
        assert calculateBonuses(STANDARD, 10000) == __multiplication(1.5, 0.5)
        assert calculateBonuses(STANDARD, 50000) == __multiplication(2, 0.5)
        assert calculateBonuses(STANDARD, 100000) == __multiplication(2.5, 0.5)

    if program == PREMIUM:
        assert calculateBonuses(PREMIUM, 1) == __multiplication(1, 0.1)
        assert calculateBonuses(PREMIUM, 10000) == __multiplication(1.5, 0.1)
        assert calculateBonuses(PREMIUM, 50000) == __multiplication(2, 0.1)
        assert calculateBonuses(PREMIUM, 100000) == __multiplication(2.5, 0.1)

    if program == DIAMOND:
        assert calculateBonuses(DIAMOND, 1) == __multiplication(1, 0.2)
        assert calculateBonuses(DIAMOND, 10000) == __multiplication(1.5, 0.2)
        assert calculateBonuses(DIAMOND, 50000) == __multiplication(2, 0.2)
        assert calculateBonuses(DIAMOND, 100000) == __multiplication(2.5, 0.2)

    if program == NOTHING_ELSE_MATTERS:
        assert calculateBonuses(" ", 1) == __multiplication(1, 0.0)
        assert calculateBonuses("cwsdafjwlkjc", 10000) == __multiplication(1.5, 0.0)
        assert calculateBonuses("wczsr", 50000) == __multiplication(2, 0.0)
        assert calculateBonuses("NOTHING", 100000) == __multiplication(2.5, 0.0)


def __multiplication(x, y):
    return x * y